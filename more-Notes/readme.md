我们的助教写了一篇中文精要，便于大家快速理解论文中的内容。 

- 下载地址：
  - 坚果云：[点击此处下载](https://www.jianguoyun.com/p/DcbuivcQtKiFCBiXt7sEIAA)
  - 码云：[点击查看](https://gitee.com/lianxh/paper/tree/master/more-Notes)

- B2-Liu-2019-AEJ-中文精要-申广军-金钊.pdf
- B4-Lu-2019-AEJ-中文精要-申广军-胡文涛.pdf
- C1-Keane-2020-QE-MO-OLS-中文精要-杨海生-王美甜.pdf
- C3-Kondor-2021-JF-Clients-中文精要-杨海生-陈卓然.pdf

Note：论文信息可以在 [课程大纲](https://gitee.com/lianxh/paper/blob/master/outline.md) 中查询。